/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {PureComponent} from 'react';
import {Platform, StyleSheet,TouchableOpacity , FlatList, Text, View} from 'react-native';
import AlarmItem from "./AlarmItem"

class App extends PureComponent{
  _onPress = () => {
    this.props.onPressItem(this.props.id);
  };

  constructor(props){
    super(props);
    this.state={
      data: [
        {
            id: '0',
            title: 'Alarma 01',
            hr: '12',
            min: '00',
            tm: 'AM',
        },
        {
            id: '1',
            title: 'Alarma 02',
            hr: '11',
            min: '30',
            tm: 'PM',
        },
        {
            id: '2',
            title: 'Alarma 03',
            hr: '9',
            min: '00',
            tm: 'PM',
        },
        
        {
            id: '3',
            title: 'Alarma 04',
            hr: '08',
            min: '00',
            tm: 'PM',
        },
      ]
      }
    
  }

  render() {
    console.warn(this.state.data);
    return (
      <FlatList
        data={this.state.data}
        renderItem={({item})=>{
          return(
            //Sin Componente
            //<Text>{item.hr} : {item.min} {item.tm}</Text>
            //Con componente
            <AlarmItem item={item}/>//le pasamos al componente AlarmItem por props el elemento iterado en item
            )
          }      
        }
        keyExtractor={(item,index)=>index.toString()}
      />

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default App;