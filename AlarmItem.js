import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

class AlarmItem extends Component{
    constructor(props){
        super(props);
       // console.warn('AlarmItem.js');
    }
    render(){
        return(
            <Text>{this.props.item.hr} : {this.props.item.min} {this.props.item.tm}</Text>
        );
    }
}
export default AlarmItem;